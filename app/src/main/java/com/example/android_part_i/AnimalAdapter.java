package com.example.android_part_i;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_part_i.model.AnimalDB;

import java.util.List;

public class AnimalAdapter extends RecyclerView.Adapter<AnimalAdapter.AnimalViewHolder> {

    List<AnimalDB> animals;

    public AnimalAdapter(List<AnimalDB> animals){
        this.animals = animals;
    }

    @NonNull
    @Override
    public AnimalAdapter.AnimalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        CardView cardView = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_animal, parent, false);

        return new AnimalViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(@NonNull AnimalAdapter.AnimalViewHolder holder, int position) {

        CardView cardView = holder.cardView;
        AnimalDB animal = animals.get(position);

        String typeOfAnimalString = animal.getTypeOfAnimal();

//      TextView typeOfAnimal = (TextView)cardView.findViewById(R.id.cv_type_of_animal);
//      typeOfAnimal.setText(typeOfAnimalString);

        TextView factAboutAnimal = (TextView)cardView.findViewById(R.id.cv_fact_about_animal);
        factAboutAnimal.setText(animal.getFactAboutAnimal());

        ImageView animalImage = (ImageView)cardView.findViewById(R.id.cv_animal_image);

        switch(typeOfAnimalString.toLowerCase()){
            case "cat":{
                animalImage.setImageResource(R.drawable.cat);
                break;
            }
            case "dog":{
                animalImage.setImageResource(R.drawable.dog);
                break;
            }
            default:
                animalImage.setImageResource(R.drawable.ic_launcher_background);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return 100;
    }

    //================================================================================//

    class AnimalViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;

        public AnimalViewHolder(@NonNull CardView itemView) {
            super(itemView);

            cardView = itemView;
        }
    }
}
