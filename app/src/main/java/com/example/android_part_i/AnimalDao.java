package com.example.android_part_i;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.android_part_i.model.AnimalDB;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class AnimalDao {

    @Insert
    public abstract void insertAll(List<AnimalDB> animals);

    @Query("SELECT * FROM AnimalDB")
    public abstract Flowable<List<AnimalDB>> selectAll();

    @Query("DELETE FROM AnimalDB")
    public abstract void removeAll();

    public void updateAll(List<AnimalDB> animals) {
        removeAll();
        insertAll(animals);
    }
}
