package com.example.android_part_i;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.android_part_i.model.AnimalDB;

@Database(entities= AnimalDB.class, version=1)
public abstract class AnimalDataBase extends RoomDatabase {
    public abstract AnimalDao getAnimalDao();
}
