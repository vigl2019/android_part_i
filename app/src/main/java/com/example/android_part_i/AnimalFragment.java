package com.example.android_part_i;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.example.android_part_i.model.AnimalDB;
import com.example.android_part_i.utils.Helper;
import com.example.android_part_i.utils.IHelper;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AnimalFragment extends Fragment {

    private AnimalDataBase dataBase;
    private List<AnimalDB> animal;
    AnimalAdapter animalAdapter;
    RecyclerView recyclerView;
    Helper helper;
    Context context;

    private IHelper iHelper = new IHelper() {
        @Override
        public void getAnimals(List<AnimalDB> animals) {
            animalAdapter = new AnimalAdapter(animals);
            recyclerView.setAdapter(animalAdapter);
        }
    };

    //================================================================================//

    public AnimalFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);

        this.context = context;
    }


    //================================================================================//

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//      return inflater.inflate(R.layout.fragment_animal, container, false);

        View root =  inflater.inflate(R.layout.fragment_animal, container, false);
        recyclerView = root.findViewById(R.id.fa_animal_list);

        dataBase = Room.databaseBuilder(context, AnimalDataBase.class, "AnimalDataBase")
                .fallbackToDestructiveMigration() // удаляет все старые таблицы и создает новые
                .build();

        helper = new Helper(context, dataBase);
        helper.getAnimalsFromServer(iHelper);
        helper.getAnimalsFromDataBase(iHelper);

        return root;
    }

    //================================================================================//

    @Override
    public void onStop() {
        super.onStop();

        helper.unSubscribe();
    }
}