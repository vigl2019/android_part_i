package com.example.android_part_i.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class ResponseModel {

    @SerializedName("all")
    @Expose
    private List<All> all = null;

    public List<All> getAll() {
        return all;
    }

    public void setAll(List<All> all) {
        this.all = all;
    }

    public class All {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("text")
        @Expose
        private String text;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("user")
        @Expose
        private User user;
        @SerializedName("upvotes")
        @Expose
        private Integer upvotes;
        @SerializedName("userUpvoted")
        @Expose
        private Object userUpvoted;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public Integer getUpvotes() {
            return upvotes;
        }

        public void setUpvotes(Integer upvotes) {
            this.upvotes = upvotes;
        }

        public Object getUserUpvoted() {
            return userUpvoted;
        }

        public void setUserUpvoted(Object userUpvoted) {
            this.userUpvoted = userUpvoted;
        }
    }

    public class Name {

        @SerializedName("first")
        @Expose
        private String first;
        @SerializedName("last")
        @Expose
        private String last;

        public String getFirst() {
            return first;
        }

        public void setFirst(String first) {
            this.first = first;
        }

        public String getLast() {
            return last;
        }

        public void setLast(String last) {
            this.last = last;
        }
    }

    public class User {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private Name name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Name getName() {
            return name;
        }

        public void setName(Name name) {
            this.name = name;
        }
    }
}