package com.example.android_part_i.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class AnimalDB {

    @PrimaryKey
    private int id;

    String typeOfAnimal;
    String factAboutAnimal;

    public AnimalDB(int id, String typeOfAnimal, String factAboutAnimal) {
        this.id = id;
        this.typeOfAnimal = typeOfAnimal;
        this.factAboutAnimal = factAboutAnimal;
    }

    //================================================================================//

    public int getId() {
        return id;
    }

    public String getTypeOfAnimal() {
        return typeOfAnimal;
    }

    public String getFactAboutAnimal() {
        return factAboutAnimal;
    }
}
