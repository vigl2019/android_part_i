package com.example.android_part_i.utils;

import com.example.android_part_i.model.AnimalDB;

import java.util.List;

public interface IHelper {
    void getAnimals(List<AnimalDB> animals);
}
