package com.example.android_part_i.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.example.android_part_i.AnimalDataBase;
import com.example.android_part_i.ApiService;
import com.example.android_part_i.R;
import com.example.android_part_i.model.AnimalDB;
import com.example.android_part_i.model.ResponseModel;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class Helper {

    private Context context;
    private CompositeDisposable disposables = new CompositeDisposable();
    private AnimalDataBase dataBase;

    public Helper(Context context, AnimalDataBase dataBase) {
        this.context = context;
        this.dataBase = dataBase;
    }

    //================================================================================//

    public void getAnimalsFromServer(final IHelper iHelper) {

        disposables.add(ApiService.getData()

                .map(new Function<ResponseModel, List<AnimalDB>>() {
                    @Override
                    public List<AnimalDB> apply(ResponseModel responseModel) {

                        final List<AnimalDB> animalDBlist = Converter.convert(responseModel);
                        dataBase.getAnimalDao().updateAll(animalDBlist);
                        return animalDBlist;
                    }
                })

                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .filter(new Predicate<List<AnimalDB>>() {
                    @Override
                    public boolean test(List<AnimalDB> animalDBlist) throws Exception {

                        boolean isAnimalDBListHaveValues = ((animalDBlist != null) && (!animalDBlist.isEmpty()));

                        if (!isAnimalDBListHaveValues) {
                            Helper.createAlertDialog(context.getResources().getString(R.string.empty_animal_db_list), null, context);
                        }

                        return isAnimalDBListHaveValues;
                    }
                })

                .subscribe(new Consumer<List<AnimalDB>>() {
                    @Override
                    public void accept(List<AnimalDB> animalDBlist) throws Exception {
                        iHelper.getAnimals(animalDBlist);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_LONG).show();
//                        Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                }));
    }

    //================================================================================//

    public void getAnimalsFromDataBase(final IHelper iHelper) {

        disposables.add(dataBase.getAnimalDao().selectAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .filter(new Predicate<List<AnimalDB>>() {
                    @Override
                    public boolean test(List<AnimalDB> animals) throws Exception {

                        boolean isAnimalDBListHaveValues = ((animals != null) && (!animals.isEmpty()));

                        if (!isAnimalDBListHaveValues) {
//                          createAlertDialog(context.getResources().getString(R.string.empty_animal_db_list), null, context);
                        }

                        return isAnimalDBListHaveValues;
                    }
                })

                .subscribe(new Consumer<List<AnimalDB>>() {
                    @Override
                    public void accept(List<AnimalDB> animals) throws Exception {
                        iHelper.getAnimals(animals);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_LONG).show();
//                        Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                }));
    }

    //================================================================================//

    public void unSubscribe() {
        disposables.clear();
    }

    //================================================================================//

    public static void createAlertDialog(String errorMassage, final View view, Context context) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle("Error of input data!")
                .setMessage(errorMassage)
                .setCancelable(false)
                .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();

                        if (view != null) {
                            view.requestFocus();
                        }
                    }
                });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }
}

