package com.example.android_part_i.utils;

import com.example.android_part_i.model.AnimalDB;
import com.example.android_part_i.model.ResponseModel;

import java.util.ArrayList;
import java.util.List;

public class Converter {

    public static List<AnimalDB> convert(ResponseModel responseModel) {

        List<AnimalDB> animals = new ArrayList<>();
        List<ResponseModel.All> all = responseModel.getAll();

        for (int i = 0; i < all.size(); i++) {

            animals.add(new AnimalDB(i + 1, all.get(i).getType(), all.get(i).getText()));
        }

        return animals;
    }
}

